///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author    Aksel Sloan <akselhawaii.edu> 
/// @date      25 January 2022  
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {       //get the terminal inputs into the code
   int i =argc-1;                   //i is one less than the number of words (NULL seperated arguements) 
   int j = 1;                       //j is the envp # we iterate over

   if (i >  0)                      //until you get to ./mouseNcat
   {

       for (i; i >0 ; i--)          //one less than the number of arguements (since ./mou is an arguement) 
      {                       //start at the last word and go backwards
         std::cout<< argv[i] <<std::endl;   //print the argument 
      }
       
   }
   else 
   {
   while(envp[j] != NULL){          //stop when you are out of things (very powerful!!!!)
      std::cout<< envp[j] <<std::endl;
      j++;                     //increase j by 1 j=j+1 
      }
   }

   std::exit( EXIT_SUCCESS );    //exit the code 
}
